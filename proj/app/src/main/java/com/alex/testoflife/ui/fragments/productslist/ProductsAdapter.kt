package com.alex.testoflife.ui.fragments.productslist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alex.testoflife.data.model.Product
import com.alex.testoflife.databinding.ProductListItemBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class ProductsAdapter(
    private var items: List<Product>?,
    private var itemClickListener: ((Product?) -> Unit?)?,
    private var imgWidth: Int
) : RecyclerView.Adapter<ProductsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ProductListItemBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding, itemClickListener, imgWidth)
    }

    override fun getItemCount(): Int = items?.size ?: 0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items?.get(position) ?: null)
    }

    fun replaceData(arrayList: List<Product>?) {
        items = arrayList
        notifyDataSetChanged()
    }


    class ViewHolder(
        private var binding: ProductListItemBinding,
        var itemClickListener: ((Product?) -> Unit?)?,
        var imgWidth: Int
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(product: Product?) {
            binding.product = product

            binding.root.setOnClickListener {
                itemClickListener?.let { it1 -> it1(product) }
            }


            if (product != null) {
                Glide.with(itemView.context).load(product.imageUrl).override(imgWidth, imgWidth)
                    .diskCacheStrategy(
                        DiskCacheStrategy.AUTOMATIC
                    )
                    .into(binding.productImg)

            };
            binding.executePendingBindings()
        }
    }

}
