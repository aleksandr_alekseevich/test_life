package com.alex.testoflife.core.di.modules

import com.alex.testoflife.ui.activity.MainActivity
import com.alex.testoflife.core.di.ActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [ContextProviderModule::class])
abstract class AppModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [ActivityModule::class])
    abstract fun contributeMainActivity(): MainActivity
}