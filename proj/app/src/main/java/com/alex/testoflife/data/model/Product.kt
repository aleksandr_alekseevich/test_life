package com.alex.testoflife.data.model

import com.google.gson.annotations.SerializedName

class Product {

    @SerializedName("product_id")
    var id: String? = null // funny id 6_id_is_a_string =))
    var name: String? = null
    var price: Double? = null
    @SerializedName("image")
    var imageUrl: String? = null
    var description: String? = null
}