package com.alex.testoflife.core.network

import retrofit2.Retrofit
import javax.inject.Inject

class RestClient {

    var apiInterface: ApiInterface

    @Inject
    constructor(retrofit: Retrofit) {
        apiInterface = retrofit.create(ApiInterface::class.java)
    }
}