package com.alex.testoflife.data.store

import com.alex.testoflife.core.localdatacache.LocalDataManager
import com.alex.testoflife.data.model.Product
import com.alex.testoflife.domain.ProductsStore
import io.reactivex.Observable
import javax.inject.Inject

class LocalProductStore @Inject constructor(private val localManager: LocalDataManager) : ProductsStore {
    override fun getAllProducts(): Observable<List<Product>> = localManager.getProducts()

    override fun getProductById(id: String): Observable<Product> = localManager.getProductById(id)
}