package com.alex.testoflife.ui.fragments.productslist

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.alex.testoflife.data.model.Product
import com.alex.testoflife.domain.ProductsRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ProductListViewModel( productsRepository: ProductsRepository) : ViewModel() {


    var isLoadingInProgress = ObservableField(true)

    private val _products = MutableLiveData<MutableList<Product>>()
    val products: LiveData<MutableList<Product>>
        get() = _products


    init {
        productsRepository.getAllProducts().subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { items -> handleResponse(items) },
                { error ->
                    run {
                        handleResponse(null)
                        error.printStackTrace()
                    }
                })
    }

    private fun handleResponse(items: List<Product>?) {
        _products.value = items as MutableList<Product>?
        isLoadingInProgress.set(false)
    }

    class Factory @Inject constructor(
        private val productsRepository: ProductsRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ProductListViewModel(productsRepository) as T
        }
    }
}