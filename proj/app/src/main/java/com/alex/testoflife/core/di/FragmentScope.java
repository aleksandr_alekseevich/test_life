package com.alex.testoflife.core.di;

import javax.inject.Scope;


@Scope
public @interface FragmentScope {
}
