package com.alex.testoflife.ui.fragments.productslist

import android.content.Context
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.alex.testoflife.R
import com.alex.testoflife.data.model.Product
import com.alex.testoflife.databinding.FragmentProductListBinding
import com.alex.testoflife.ui.fragments.productdescription.ProductDescriptionFragment.Companion.ARG_PRODUCT_ID
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject


class ProductsListFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ProductListViewModel.Factory

    lateinit var viewModel: ProductListViewModel
    lateinit var binding: FragmentProductListBinding

    lateinit var adapter: ProductsAdapter//(arrayListOf(), ::itemClicked)

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_product_list,
            container,
            false
        )

        val displaymetrics = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(displaymetrics)
        val width = displaymetrics.widthPixels

        adapter = ProductsAdapter(arrayListOf(), ::itemClicked, width / 2)


        binding.productsList.layoutManager =
            StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        binding.productsList.adapter = adapter

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel =
            ViewModelProviders.of(this, viewModelFactory).get(ProductListViewModel::class.java)

        binding.viewModel = viewModel
        viewModel.products.observe(
            this.viewLifecycleOwner,
            Observer<List<Product>> { p -> adapter.replaceData(p) })
    }

    private fun itemClicked(product: Product?) {
        var bundle = bundleOf(ARG_PRODUCT_ID to product?.id)
        findNavController().navigate(
            R.id.action_productsListFragment_to_productDescriptionFragment,
            bundle
        )
    }
}