package com.alex.testoflife.data

import com.alex.testoflife.core.network.NetworkManager
import com.alex.testoflife.data.model.Product
import com.alex.testoflife.data.store.ProductStoreFactory
import com.alex.testoflife.domain.ProductsRepository
import io.reactivex.Observable
import javax.inject.Inject

class ProductsDataRepository @Inject constructor(private val productStoreFactory: ProductStoreFactory) :
    ProductsRepository {


    override fun getAllProducts(): Observable<List<Product>> {
        return productStoreFactory.getSoreForAllProducts().getAllProducts()
    }

    override fun getProductById(id: String): Observable<Product> {
        return productStoreFactory.getStoreForExactProduct(id).getProductById(id)
    }
}