package com.alex.testoflife.core.network

import com.alex.testoflife.data.model.Product
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {

    @GET("list")
    fun getAllProducts(): Observable<ProductResponseModel>

    @GET("{product_id}/detail")
    fun getProductById(@Path("product_id") id: String): Observable<Product>
}