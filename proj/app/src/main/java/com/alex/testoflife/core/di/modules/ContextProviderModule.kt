package com.alex.testoflife.core.di.modules

import android.content.Context
import com.alex.testoflife.LifeApplication
import dagger.Module
import dagger.Provides

@Module
class ContextProviderModule {
    @Provides
    fun provideContext(app: LifeApplication): Context = app.applicationContext
}