package com.alex.testoflife.data.store

import com.alex.testoflife.core.network.NetworkManager
import com.alex.testoflife.data.model.Product
import com.alex.testoflife.domain.ProductsStore
import io.reactivex.Observable
import javax.inject.Inject

class NetworkProductStore @Inject constructor(private val networkManager: NetworkManager) :
    ProductsStore {
    override fun getAllProducts(): Observable<List<Product>> {
        return networkManager.getProducts()
    }

    override fun getProductById(id: String): Observable<Product> {
        return networkManager.getProductById(id)
    }
}