package com.alex.testoflife.ui.fragments.productdescription

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.alex.testoflife.data.model.Product
import com.alex.testoflife.domain.ProductsRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class ProductDescriptionViewModel(private val productsRepository: ProductsRepository) :
    ViewModel() {

    var isLoadingInProgress = ObservableField(true)

    var product = ObservableField<Product>()

    private val _imgUrl = MutableLiveData<String>()
    val imgUrl: LiveData<String>
        get() = _imgUrl


    fun loadProductWithId(id: String) {
        productsRepository.getProductById(id).subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { items -> handleResponse(items) },
                { error ->
                    run {
                        handleResponse(null)
                        error.printStackTrace()
                    }
                })
    }

    private fun handleResponse(p: Product?) {
        product.set(p)
        _imgUrl.value = p?.imageUrl
        isLoadingInProgress.set(false)
    }

    class Factory @Inject constructor(
        private val productsRepository: ProductsRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ProductDescriptionViewModel(productsRepository) as T
        }
    }
}