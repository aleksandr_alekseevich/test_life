package com.alex.testoflife.core.di.modules

import com.alex.testoflife.core.di.FragmentScope
import com.alex.testoflife.ui.fragments.productdescription.ProductDescriptionFragment
import com.alex.testoflife.ui.fragments.productslist.ProductsListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeListFragment(): ProductsListFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeDescriptionFragment(): ProductDescriptionFragment
}