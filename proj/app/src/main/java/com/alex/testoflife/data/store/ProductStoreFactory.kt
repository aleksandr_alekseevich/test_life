package com.alex.testoflife.data.store

import com.alex.testoflife.core.localdatacache.LocalDataManager
import com.alex.testoflife.core.network.NetworkManager
import com.alex.testoflife.domain.ProductsStore
import javax.inject.Inject

class ProductStoreFactory @Inject constructor(
    val networkmanager: NetworkManager,
    val cacheManager: LocalDataManager
) {

    fun getSoreForAllProducts(): ProductsStore {
        return if (cacheManager.listProducts != null) {
            LocalProductStore(cacheManager)
        } else {
            NetworkProductStore(networkmanager)
        }
    }

    fun getStoreForExactProduct(id: String): ProductsStore {
        return if (cacheManager.isFullProductExist(id)!!) {
            LocalProductStore(cacheManager)
        } else {
            NetworkProductStore(networkmanager)
        }
    }

}