package com.alex.testoflife.domain

import com.alex.testoflife.data.model.Product
import io.reactivex.Observable


interface ProductsRepository {

    fun getAllProducts(): Observable<List<Product>>

    fun getProductById(id: String): Observable<Product>
}