package com.alex.testoflife.core.di.components

import com.alex.testoflife.LifeApplication
import com.alex.testoflife.core.di.modules.AppModule
import com.alex.testoflife.core.di.modules.ComponentsModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidInjectionModule::class, AppModule::class, ComponentsModule::class]
)
interface AppComponent {

    fun inject(application: LifeApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: LifeApplication): Builder

        fun build(): AppComponent
    }
}