package com.alex.testoflife.core.network

import com.alex.testoflife.core.localdatacache.LocalDataManager
import com.alex.testoflife.data.model.Product
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkManager @Inject constructor(
    private var restClient: RestClient,
    private var localManager: LocalDataManager
) {

    fun getProducts(): Observable<List<Product>> {
        return restClient.apiInterface.getAllProducts().map { item ->
            if (item?.products != null) {
                localManager.listProducts = item.products
                return@map item.products
            }
            return@map null
        } //todo try to resend request if some problem
    }

    fun getProductById(id: String): Observable<Product> {
        return restClient.apiInterface.getProductById(id)
            .doOnNext { if (it != null) localManager.addFullProduct(it) }
    }
}