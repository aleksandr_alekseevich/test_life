package com.alex.testoflife.core.localdatacache

import com.alex.testoflife.data.model.Product
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class LocalDataManager @Inject constructor() {

    var listProducts: List<Product>? = null

    val hashMap: HashMap<String, Product> = HashMap()

    // FIXME it is not good implementation. But enough to show for test app

    fun isFullProductExist(id: String): Boolean {
        return hashMap[id] != null
    }

    fun addFullProduct(product: Product) {
        product.id?.let { hashMap.put(it, product) }
    }

    fun getProducts(): Observable<List<Product>> {
        return Observable.just(listProducts)
    }

    fun getProductById(id: String): Observable<Product> {
        return Observable.just(hashMap[id])
    }
}