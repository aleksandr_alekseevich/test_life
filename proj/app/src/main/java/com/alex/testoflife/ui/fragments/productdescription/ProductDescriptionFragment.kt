package com.alex.testoflife.ui.fragments.productdescription

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.alex.testoflife.R
import com.alex.testoflife.databinding.FragmentProductDescriptinBinding
import com.bumptech.glide.Glide
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class ProductDescriptionFragment : Fragment() {

    companion object {
        const val ARG_PRODUCT_ID = "ARG_PRODUCT_ID"
    }

    @Inject
    lateinit var viewModelFactory: ProductDescriptionViewModel.Factory

    lateinit var viewModel: ProductDescriptionViewModel
    lateinit var binding: FragmentProductDescriptinBinding

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_product_descriptin,
            container,
            false
        )

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel =
            ViewModelProviders.of(this, viewModelFactory)
                .get(ProductDescriptionViewModel::class.java)

        arguments?.getString(ARG_PRODUCT_ID)?.let { viewModel.loadProductWithId(it) }

        binding.viewModel = viewModel
        viewModel.imgUrl.observe(
            this.viewLifecycleOwner,
            Observer<String?> { url ->
                url?.let {
                    Glide.with(context!!).load(it).into(binding.productImg)
                }
            })
    }

}